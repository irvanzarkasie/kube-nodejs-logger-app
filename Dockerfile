FROM node:8-alpine

RUN npm install express

RUN npm install winston

RUN npm install dotenv

COPY env.docker .env

COPY kube-logger-app.js .

CMD node kube-logger-app.js >> ./logs/kube-logger-app.out

EXPOSE 3001
